package com.example.catsapi.data.apiservice.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class BreedDto(
    @SerialName("weight") val weight : CatWeightDto,
    @SerialName("id") var id: String,
    @SerialName("name") var name: String,
    @SerialName("cfa_url") var cfaUrl: String,
    @SerialName("vetstreet_url") var vetstreetUrl: String,
    @SerialName("vcahospitals_url") var vcahospitalsUrl: String,
    @SerialName("temperament") var temperament: String,
    @SerialName("origin") var origin: String,
    @SerialName("country_codes") var countryCodes: String,
    @SerialName("country_code") var countryCode: String,
    @SerialName("description") var description: String,
    @SerialName("life_span") var lifeSpan: String,
    @SerialName("indoor") var indoor: Int,
    @SerialName("lap") var lap: Int,
    @SerialName("alt_names") var altNames: String,
    @SerialName("adaptability") var adaptability: Int,
    @SerialName("affection_level") var affectionLevel: Int,
    @SerialName("child_friendly") var childFriendly: Int,
    @SerialName("dog_friendly") var dogFriendly: Int,
    @SerialName("energy_level") var energyLevel: Int,
    @SerialName("grooming") var grooming: Int,
    @SerialName("health_issues") var healthIssues: Int,
    @SerialName("intelligence") var intelligence: Int,
    @SerialName("shedding_level") var sheddingLevel: Int,
    @SerialName("social_needs") var socialNeeds: Int,
    @SerialName("stranger_friendly") var strangerFriendly: Int,
    @SerialName("vocalisation") var vocalisation: Int,
    @SerialName("experimental") var experimental: Int,
    @SerialName("hairless") var hairless: Int,
    @SerialName("natural") var natural: Int,
    @SerialName("rare") var rare: Int,
    @SerialName("rex") var rex: Int,
    @SerialName("suppressed_tail") var suppressedTail: Int,
    @SerialName("short_legs") var shortLegs: Int,
    @SerialName("wikipedia_url") var wikipediaUrl: String,
    @SerialName("hypoallergenic") var hypoallergenic: Int,
    @SerialName("reference_image_id") var referenceImageId: String
)