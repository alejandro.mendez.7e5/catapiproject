package com.example.catsapi.data.apiservice.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CatWeightDto(
    @SerialName("imperial") var imperial: String,
    @SerialName("metric") var metric: String
)