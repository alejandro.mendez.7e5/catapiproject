package com.example.catsapi.data.apiservice
import com.example.catsapi.data.apiservice.model.BreedDto
import com.example.catsapi.data.apiservice.model.CatImageDto
import retrofit2.Retrofit
import retrofit2.http.GET
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Query

private const val BASE_URL = "https://api.thecatapi.com"
private val retrofit = Retrofit.Builder()
    .addConverterFactory(GsonConverterFactory.create())
    .baseUrl(BASE_URL)
    .build()

interface CatApiService {
    @GET("./v1/breeds")
    suspend fun getBreed() : List<BreedDto>
    @GET("./v1/images")
    suspend fun getPhotos(@Query("breed_id")id:String) : List<CatImageDto>
}

object CatsApi {
    val retrofitService : CatApiService by lazy {
        retrofit.create(CatApiService::class.java)
    }
}
