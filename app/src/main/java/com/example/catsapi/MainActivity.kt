package com.example.catsapi

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.catsapi.ViewModel.CatsViewModel
import com.example.catsapi.ui.model.CatUIModel
import com.example.catsapi.ui.theme.CatsAPITheme

import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Info
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextOverflow


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CatApp()
        }
    }
}

@Composable
fun CatApp(catViewModel: CatsViewModel = viewModel()) {
    val uiState by catViewModel.catsUIState.collectAsState()
    uiState?.let {
        CatList(catsList = it)
    }
}

@Composable
fun CatList(catsList: List<CatUIModel>) {
    LazyColumn() {
        this.items(items = catsList, itemContent = { item ->
            CatCard(cat = item)
        })
    }
}

@Composable
fun CatCard(cat : CatUIModel, modifier: Modifier = Modifier) {
    val expanded = remember { mutableStateOf(false) }
    Card(
        border = BorderStroke(4.dp, MaterialTheme.colors.primary)
    ) {
        Column(
            modifier = Modifier
                .animateContentSize(
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioMediumBouncy,
                        stiffness = Spring.StiffnessLow
                    )

                )
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp)

            ) {
                Image(
                    painter = rememberAsyncImagePainter("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTia7DkftuxaVa11VQSqAD0o-lyedBYgVBCdg&usqp=CAU"),
                    contentDescription = "Imagen de muestra",
                    modifier = Modifier
                        .size(64.dp)
                        .padding(8.dp)
                        .clip(RoundedCornerShape(50.dp)),
                    contentScale = ContentScale.Crop
                )
                Spacer(Modifier.padding(40.dp))
                Text(
                    text = cat.name,
                    fontSize = 13.sp,
                    modifier = Modifier.weight(1f)
                )
                DogItemButton(
                    expanded = expanded.value,
                    onClick = {
                        expanded.value = !expanded.value
                    }
                )
            }
            if (expanded.value) {
                Description(cat)
            }
        }
    }
}
@Composable
private fun DogItemButton(
    expanded: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    IconButton(onClick = onClick) {
        Icon(
            imageVector =
            if (expanded)
                Icons.Filled.Close
            else
                Icons.Filled.Info,
            tint = MaterialTheme.colors.secondary,
            contentDescription = "Boton desplegable",
            modifier = Modifier.requiredWidth(50.dp)
        )

    }
}

@Composable
private fun Description(cat : CatUIModel, modifier: Modifier = Modifier) {
    val context = LocalContext.current
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxSize()
            .padding(8.dp)
    ) {
        Column(
            modifier = Modifier.padding(
                start = 16.dp,
                top = 8.dp,
                bottom = 16.dp,
                end = 16.dp
            )
        ) {
            Text(
                text = "Descripcion: ",
                fontSize = 13.sp,
            )
            Text(
                text = cat.description,
                fontSize = 15.sp,
                modifier = Modifier
                    .padding(8.dp),
                overflow = TextOverflow.Ellipsis,
                maxLines = 2
            )
        }
    }
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxSize()
            .padding(4.dp)
    ) {
        Spacer(Modifier.padding(10.dp))

        val context = LocalContext.current
        TextButton(onClick = {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("cat", cat)
            context.startActivity(intent)
        }) {
            Text("See more")
        }
    }
}