package com.example.catsapi


import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.example.catsapi.ui.model.CatUIModel

class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val cat = intent.getParcelableExtra<CatUIModel>("cat")

        setContent {
            CatDetail(cat!!)
        }
    }
}

@Composable
fun CatDetail(cat : CatUIModel){
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxWidth()
        ) {
            Image(
                painter = rememberAsyncImagePainter("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTia7DkftuxaVa11VQSqAD0o-lyedBYgVBCdg&usqp=CAU"),
                contentDescription = "image",
                modifier = Modifier
                    .padding(16.dp)
                    .size(200.dp)
                    .clip(RoundedCornerShape(50)),
                contentScale = ContentScale.Crop,

                )
            Text(
                text = cat.name,
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.ExtraBold,
                style = MaterialTheme.typography.h5,
                modifier = Modifier.padding(16.dp)
            )
            Text(
                text = cat.description,
                style = MaterialTheme.typography.body2,
                modifier = Modifier.padding(16.dp)
            )
            if( cat.codCountry != null){
                Text(
                    text = cat.codCountry,
                    style = MaterialTheme.typography.body2,
                    modifier = Modifier.padding(16.dp)
                )
            }
            Text(
                text = cat.temperamnet,
                style = MaterialTheme.typography.body2,
                modifier = Modifier.padding(16.dp)
            )
            if( cat.wikiURL != null){
                Text(
                    text = cat.wikiURL,
                    style = MaterialTheme.typography.body2,
                    modifier = Modifier.padding(16.dp)
                )
            }

    }
}