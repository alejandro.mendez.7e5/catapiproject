package com.example.catsapi.ui.model.mappers

import com.example.catsapi.data.apiservice.model.BreedDto
import com.example.catsapi.data.apiservice.model.CatImageDto
import com.example.catsapi.ui.model.CatUIModel

class CatsDtoUIModelMapper {
    /*
    fun map(listBreeds : List<BreedDto>, listImages : List<CatImageDto>): List<CatUIModel>{
        return mutableListOf<CatUIModel>().apply {
            listBreeds.forEachIndexed{index, breed ->
                CatUIModel(
                    url = listImages[index].url,
                    name = breed.name,
                    description = breed.description,
                    codCountry = breed.countryCode,
                    temperamnet = breed.temperament,
                    wikiURL = breed.wikipediaUrl
                )
            }
        }
    }*/

    fun map(listBreeds : List<BreedDto>, listImages : List<CatImageDto>): List<CatUIModel>{
        return listBreeds.mapIndexed { index, breed ->
            CatUIModel(
                url = "",
                name = breed.name,
                description = breed.description,
                codCountry = breed.countryCode,
                temperamnet = breed.temperament,
                wikiURL = breed.wikipediaUrl
            )
        }
        return mutableListOf<CatUIModel>().apply {
            listBreeds.forEachIndexed{
                    index, breed ->
                add(CatUIModel(
                    url = listImages[index].url,
                    name = breed.name,
                    description = breed.description,
                    codCountry = breed.countryCode,
                    temperamnet = breed.temperament,
                    wikiURL = breed.wikipediaUrl
                ))
            }
        }
    }
}