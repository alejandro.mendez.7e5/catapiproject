package com.example.catsapi.ui.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CatUIModel (
    val url : String,
    val name : String,
    val description : String,
    val codCountry: String ? = "",
    val temperamnet : String,
    val wikiURL : String ? = ""
) :Parcelable
