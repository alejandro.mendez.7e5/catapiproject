package com.example.catsapi.ViewModel

import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import com.example.catsapi.ui.model.CatUIModel
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.catsapi.data.apiservice.CatsApi
import com.example.catsapi.ui.model.mappers.CatsDtoUIModelMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class CatsViewModel : ViewModel(){

    private val _catsUIState = MutableStateFlow<List<CatUIModel>?>(null)
    val catsUIState: StateFlow<List<CatUIModel>?> = _catsUIState.asStateFlow()

    var mapper = CatsDtoUIModelMapper()

    init {
        getCats()
    }

    fun getCats() {
        viewModelScope.launch {
            val breedListDto = CatsApi.retrofitService.getBreed()
            //val imageListDto = breedListDto.flatMap { CatsApi.retrofitService.getPhotos(it.id) }
            _catsUIState.value = mapper.map(breedListDto, emptyList())
        }
    }
}